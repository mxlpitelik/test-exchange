
const rates =  [
    {
        currency1: 'BTC',
        currency2: 'ETH',
        rate: 200
    },
    {
        currency1: 'BTC',
        currency2: 'USDT',
        rate: 20000
    },  {
        currency1: 'ETH',
        currency2: 'USDT',
        rate: 1400
    }, {
        currency1: 'USDT',
        currency2: 'USDC', 
        rate: 1
    }];

/**
 * method for search ways to exchange
 */
 const findedWays = [];
const waysSearcher = (src, dst, excluded = []) => {
    
    
    rates.forEach((r, index) => {
        if(!excluded.includes(index)) {

            let result;

            if(r.currency1 === src) {
                if(r.currency2 === dst) {
                    result = [...excluded, index];
                } else {
                    result = waysSearcher(r.currency2, dst, [...excluded, index])
                }
            } else if(r.currency2 === src) {
                if(r.currency1 === dst) {
                    result = [...excluded, index];
                } else {
                    result = waysSearcher(r.currency1, dst, [...excluded, index])
                }
            }

            // console.log(src, dst, ' ==> way: ', excluded, 'result: ', result)
            if(result?.length) {
                const last = rates[result[result.length-1]];
                if(last?.currency1 === dst || last?.currency2 === dst) {
                    findedWays.push(result);
                }
            }

        }
        
    });    

    return findedWays;

};

const bestRates = (sum, src, dst) => {
    const ways =  waysSearcher(src, dst);

    const waysCost = ways.map((w, index) => {
        let sumRate = sum;
        let pairs = [];
        w.map((pairIndex) => {
            const r = rates[pairIndex];
            sumRate = sumRate * r.rate;
            pairs.push(`${r.currency1}_${r.currency2}`);
        });
        return {sumRate, index, pairs: pairs.join(', ')};
    });

    const rating = waysCost.sort((a,b) => a.sumRate - b.sumRate);

    return rating;
} 

console.log(bestRates(1, 'BTC', 'USDC'));
